#!/usr/bin/env

import csv, pandas as pd, numpy as np, matplotlib.pyplot as plt, sys
from sklearn import svm
from sklearn.metrics import confusion_matrix

def binarize_continious_data(data):
    #This fuction I create multiple ranges for all continious variables and each age to that group. 
    #After doing this I drop the existing column
    age_group = []
    for age in data["age"]:
        if age < 25:
            age_group.append("<25")
        elif 25 <= age <= 34:
            age_group.append("25-34")
        elif 34 < age <= 44:
            age_group.append("35-44")
        elif 44 < age <= 54:
            age_group.append("45-54")
        elif 54 < age <= 65:
            age_group.append("55-64")
        else:
            age_group.append("65 and over")

    income_df = data.copy()
    income_df["age_group"] = age_group
    del income_df["age"]
    
    workhours_group = []
    for workhours in income_df["workhours"]:
        if workhours < 25:
            workhours_group.append("<25")
        elif 26 <= workhours <= 50:
            workhours_group.append("26-50")
        elif 50 < workhours <= 75:
            workhours_group.append("51-75")
        else:
            workhours_group.append("76 and over")

    new_income_df = income_df.copy()
    new_income_df["workhours_group"] = workhours_group
    del new_income_df["workhours"]
    
    return new_income_df

def remove_features(data):
    data = data.replace(0, np.nan)
    data = data.dropna(axis=1, thresh=int(0.02*len(data.values))) #ignoring the column if it has only 1's that are less than 2% of the data
    data = data.replace(np.nan, 0)
    return data

def data_preprocessing(data):
    #Here I binarize all the continious and catagorical data. As I am binarizing I don't need a scalar fit.
    #I also return the values of x_train and y_train.
    #Also converting the 0 or 1 class in y_train to -1 or 1.
    data = binarize_continious_data(data)
    data = pd.get_dummies(data, drop_first=True)
    data = remove_features(data)
    return data.loc[:, data.columns != 'income_ >50K'], data['income_ >50K'].map({0: -1, 1: 1})

def SVM_c(n, train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all, kernel='linear', degree = None,):
    if degree == None:
        clf = svm.SVC(C = 10**(i_p-4), kernel=kernel)
    else:
        clf = svm.SVC(C = 10**(i_p-4), kernel=kernel, degree=degree)
    clf.fit(x_train, y_train)
    train_accuracy_all.append(clf.score(x_train, y_train))
    test_accuracy_all.append(clf.score(x_test, y_test))
    dev_accuracy_all.append(clf.score(x_dev, y_dev))
    sv_count_negative_all.append(clf.n_support_[0])
    sv_count_positive_all.append(clf.n_support_[1])
    return train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all

def kernelized_perceptron(iterations, X, Y):
    print(X.shape[0])
    alpha = np.zeros(X.shape[0])
    K = np.zeros((X.shape[0], X.shape[0]))

    mistakes = []
    mistake_count = 0

    K = (1 + np.dot(X, X.T)) ** 2

    for i in range(iterations):
        count = 0
        for x, y in zip(X.values, Y.values):
            
            y_hat = np.sign(np.sum(alpha * Y.values * K[:][count]))

            if y_hat != y:
                alpha[count] = alpha[count] + 1
                mistake_count += 1
            count += 1
            
        mistakes.append(mistake_count)
        mistake_count = 0
        
    return alpha, mistakes

def kernelized_perceptron_prediction(alpha, X, Y):
    for x in X.values:
        y_hat = np.sign(np.sum(alpha * Y.values * ((1 + np.dot(X.values, X)) ** 2)))
    correct_train = np.sum((y_hat == np.array(Y)).astype(float))
    accuracy = round((correct_train / X.shape[0]) * 100, 2)    
    return accuracy

nnp.random.seed(10)

#importing data
cols = ["age", "workclass", "education", "relationship", "profession", "race", "gender", "workhours", "nationality", "income"]
dev_data = pd.read_csv('income.dev.txt', header = None, names = cols)
test_data = pd.read_csv('income.test.txt', header = None, names = cols)
train_data = pd.read_csv('income.train.txt', header = None, names = cols)

#Doing the data preprocessing over the given data
data = pd.concat([train_data, dev_data, test_data])
X, Y = data_preprocessing(data)

x_train, y_train = X[:len(train_data)], Y[:len(train_data)]
x_dev, y_dev = X[len(train_data):len(dev_data)+len(train_data)], Y[len(train_data):len(dev_data)+len(train_data)]
x_test, y_test = X[len(dev_data)+len(train_data):], Y[len(dev_data)+len(train_data):]

print('1) The number of features = ' + str(len(X.columns)))

C = [10**i for i in range(-4,5)]

train_accuracy = []
test_accuracy = []
dev_accuracy = []
sv_count_negative = []
sv_count_positive = []

for i in C:
    classifier = svm.SVC(C = i, kernel='linear')
    classifier.fit(x_train, y_train)
    train_accuracy.append(classifier.score(x_train, y_train))
    test_accuracy.append(classifier.score(x_test, y_test))
    dev_accuracy.append(classifier.score(x_dev, y_dev))
    sv_count_negative.append(classifier.n_support_[0])
    sv_count_positive.append(classifier.n_support_[1])

print('1) The accuracies for training data is = ' + str(train_accuracy))
print('\n\tThe accuracies for dev data is = ' + str(dev_accuracy))
print('\n\tThe accuracies for test data is = ' + str(test_accuracy))

x_train_dev = pd.concat([x_train, x_dev])
y_train_dev = pd.concat([y_train, y_dev])

_, i_p = max((dev_accuracy[i],i) for i in range(len(dev_accuracy)))
print('The best power is = 10^' + str(i_p))
clf = svm.SVC(C = 10**(i_p-4), kernel='linear')
clf.fit(x_train_dev, y_train_dev)
combined_testing_accuracy = clf.score(x_test, y_test)
print('2) The combines tesing accuracy is = ' + str(combined_testing_accuracy))
predictions = clf.predict(x_test)

train_accuracy_all = []
test_accuracy_all = []
dev_accuracy_all = []
sv_count_negative_all = []
sv_count_positive_all = []

train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all = SVM_c(i_p-4, [], [], [], [], [])
train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all = SVM_c(i_p-4, train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all, 'poly', 2)
train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all = SVM_c(i_p-4, train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all, 'poly', 3)
train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all = SVM_c(i_p-4, train_accuracy_all, test_accuracy_all, dev_accuracy_all, sv_count_negative_all, sv_count_positive_all, 'poly', 4)

alpha , kernelized_mistakes = kernelized_perceptron(5, x_train, y_train)
kernelized_training_accuracy = kernelized_perceptron_prediction(alpha, x_train, y_train)
kernelized_testing_accuracy = kernelized_perceptron_prediction(alpha, x_test, y_test)
kernelized_dev_accuracy = kernelized_perceptron_prediction(alpha, x_dev, y_dev)

iterations = [1,2,3,4,5]
plt.plot(iterations, kernel_mistakes, color='black', marker='o')
plt.xlabel('No. of iterations')
plt.ylabel('No. of mistakes')
plt.title('No. of mistakes on training set as a function of the iterations')
plt.show()